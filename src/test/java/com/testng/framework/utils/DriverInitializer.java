package com.testng.framework.utils;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class DriverInitializer {
	public AndroidDriver<AndroidElement> getDriver() {
		if (DriverManager.getDriver() == null) {
			DriverClass driverclass = DriverFactory.createInstance();
			DriverManager.setDriver(driverclass);
		}
		return DriverManager.getDriver().androidDriver;
	}

	public static void quitBrowser() {
		DriverManager.getDriver().androidDriver.quit();
		DriverManager.setDriver(null);
	}
}
