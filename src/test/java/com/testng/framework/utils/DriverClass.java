package com.testng.framework.utils;

import java.net.MalformedURLException;
import java.net.URL;
import org.openqa.selenium.remote.DesiredCapabilities;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public class DriverClass {
	AndroidDriver<AndroidElement> androidDriver;
	
	private final String ANDROID_HOSTNAME = "http://127.0.0.1.4723";

	public DriverClass createDriver() {
		String driverName = PropertyReader.getProperty("androidDriver.driver");
		if (androidDriver == null && driverName.equals("ANDROID")) {
			DesiredCapabilities dc = new DesiredCapabilities();
			dc.setCapability("deviceName", PropertyReader.getProperty("deviceName"));
			dc.setCapability("platformName", PropertyReader.getProperty("platformName"));
			dc.setCapability("appPackage", PropertyReader.getProperty("appPackage"));
			dc.setCapability("appActivity", PropertyReader.getProperty("appActivity"));
			dc.setCapability("noReset", true);
			try {
				androidDriver = new AndroidDriver<AndroidElement>(new URL(ANDROID_HOSTNAME), dc);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		}
		return this;
	}

}
