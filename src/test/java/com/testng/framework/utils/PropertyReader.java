package com.testng.framework.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.Properties;

public class PropertyReader {
	
	public static String getProperty(String key) {
		Properties prop = new Properties();
		try(InputStream in = new FileInputStream(Paths.get(".")+"/config.properties")){
			prop.load(in);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return prop.getProperty(key);
	}	
	}
